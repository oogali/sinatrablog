require 'bundler'
Bundler.require

require 'sinatrablog'

module SinatraBlog
  class Application < Sinatra::Application
    include ActionView::Helpers::DateHelper
    register Sinatra::Async

    configure :development do
      enable :show_settings
    end

    use Rack::Session::Pool, :path => '/', :secret => 'SET_YOUR_SECRET_SESSION_KEY_HERE', :key => 'SESSIONID', :sidbits => 128
    use Rack::Flash

    def initialize
      super

      @settings = {}
      Model::Setting.all.each do |setting|
        @settings[setting.key] = setting.value
      end

      @title = @settings['title'] || 'Sinatra Blog'
      @page_length = @settings['page_length'] || 5
    end

    helpers do
      def partial(page, options = {})
        render :haml, page, options.merge!(:layout => false)
      end

      def title
        @post ? "#{@title}: #{@post.title}" : @title
      end

      def ago(time)
        case Time.now - time
          when 0..6.hours
            "#{time_ago_in_words time} ago"
          when 6.hours..28.hours
            time.strftime('%l:%M%p').downcase
          when 28.hours..8.months
            time.strftime('%B %d')
          else
            time.strftime('%B %d, %Y')
        end
      end
    end

    aget '/healthcheck' do
      body { "OK\n" }
    end

    get '/' do
      @posts = Model::Post.published.paginate(:page => params['page'], :per_page => (params['len'] || @page_length)).order('published_at DESC, updated_at DESC, created_at DESC')
      body { haml :index }
    end

    get '/post/new' do
      body { haml :post_new }
    end

    post '/post/new' do
      @post = Model::Post.new({
        :title => params[:title],
        :slug => params[:slug],
        :content => params[:content],
        :published_at => params[:publish] || Time.now
      })

      @post.save
      redirect @post.url
    end

    get '/post/:pid/edit' do |pid|
      @post = Model::Post.find pid
      body { haml :post_edit }
    end

    get %r{/post/(\d+)$} do |pid|
      @post = Model::Post.find pid
      body { haml :post }
    end

    get '/post/:pid' do |slug|
      @post = Model::Post.find_by_slug slug
      body { haml :post }
    end

    post %r{/post/(\d+)$} do |pid|
      post = Model::Post.find pid
      post.update_attributes({
        :title => params[:title],
        :slug => params[:slug],
        :content => params[:content],
        :published_at => params[:publish]
      })

      post.save
      redirect post.url
    end

    post %r{/post/(\d+)/comment$} do |pid|
      post = Model::Post.find pid
      comment = post.comments.new({
        :author => params[:author] || 'Anonymous',
        :email => params[:email] || 'anon@email',
        :text => params[:comment]
      })

      comment.save
      redirect post.url + "#comment_#{comment.id}"
    end

    aget %r{/css/(default|reset)\.css} do |css|
      content_type 'text/css', :charset => 'utf-8'
      body { sass :"#{css}" }
    end
  end
end
