require 'active_record'
require 'will_paginate'
require 'will_paginate/active_record'
require 'will_paginate/array'

module SinatraBlog
  module Model
    class Setting < ActiveRecord::Base
    end

    class Post < ActiveRecord::Base
      has_many :comments
      scope :published, lambda { where('posts.published_at IS NOT NULL AND posts.published_at <= now()') }

      def url
        "/post/#{self.slug || self.id}"
      end

      def summary
        if summarized?
          (words + [ '...' ]).join(' ')
        else
          self.content
        end
      end

      def words
        self.content.split /\s+/
      end

      def summarized?
        words.length > 200
      end
    end

    class Comment < ActiveRecord::Base
      belongs_to :post

      def edited?
        self.created_at != self.updated_at
      end
    end
  end
end
