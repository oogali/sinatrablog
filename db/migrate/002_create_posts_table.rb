class CreatePostsTable < ActiveRecord::Migration
  def self.up
    create_table :posts do |t|
      t.primary_key :id
      t.string :title, :null => false
      t.string :slug, { :unique => true, :null => false }
      t.text :content
      t.timestamps
      t.timestamp :published_at
    end
  end

  def self.down
    drop_table :posts
  end
end
