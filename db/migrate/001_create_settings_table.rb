class CreateSettingsTable < ActiveRecord::Migration
  def self.up
    create_table :settings do |t|
      t.primary_key :id
      t.string :key, { :unique => true, :null => false }
      t.string :value, :null => false
      t.timestamps
    end
  end

  def self.down
    drop_table :settings
  end
end
