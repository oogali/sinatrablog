class CreateCommentsTable < ActiveRecord::Migration
  def self.up
    create_table :comments do |t|
      t.primary_key :id
      t.references :post
      t.string :author, :null => false
      t.string :email, :null => false
      t.text :text
      t.timestamps
    end
  end

  def self.down
    drop_table :comments
  end
end
